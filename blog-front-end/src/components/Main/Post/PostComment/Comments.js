import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core';
import moment from 'moment'

const useStyle = makeStyles( theme => ({
    commentBlock: {
        backgroundColor: '#F4F4F4F4',
        borderRadius: '0.3rem',
        padding: '0.5rem 0.8rem',
        minHeight: '50px',
        marginBottom: '1rem',
        whiteSpace: 'break-spaces'
    },
    title: {
        
    }
}))

export const Comments = ({postId, posts}) => {
    const [currentPost, setCurrentPost] = useState([]);

    const classes = useStyle();

    useEffect(() => {
        const post = posts.filter(p => p.Id === parseInt(postId));
        if(post.length !== 0) {
            setCurrentPost(post[0].Comments);
        }
    }, [posts]);
    return (
        <>
            {
                currentPost !== [] || currentPost === undefined ?
                currentPost.map(c => (
                    <div key={c.Id} className={classes.commentBlock}>
                        <div className="row">
                            <div className="col-6">
                                From: <b>{c.Name}</b>
                            </div>
                            <div className="col-6">At: <i>{moment(c.CreatedTime.substring(0,22)).format('HH:mm:ss | DD-MMM-YYYY')}</i></div>
                            
                        </div>
                        <hr></hr>
                        <div className="row">
                            <div className="col-12">
                                {c.Comment}
                            </div>
                        </div>
                    </div>
                )) : 
                null
            }
        </>
    )
}

const mapStateToProps = (state) => ({
    posts: state.post.posts
});

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(Comments)
