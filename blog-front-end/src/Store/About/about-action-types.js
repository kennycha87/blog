export const TYPES = {
    LOAD_ABOUT: 'Load the about page content',
    SET_MD_DATA: 'Set the md data to reducer',
    SET_ERR: 'Read Err message',
    SET_SUCCESS: 'Read success message',
    SET_PICTURE: 'Set Picture',
    SET_UPLOAD_PICTURE: 'Set the picture for upload',
    CLEAR_UPLOAD_PICTURE: 'Clear the upload picture',
    SET_PATH: 'Set the server image path'
}