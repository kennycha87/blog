import React, {useEffect, useState} from 'react';
import { connect } from 'react-redux';
import { Form } from 'react-bootstrap';
import PostRoute from './PostRuote';

export const SearchPost = ({ posts }) => {
    const [keywords, setKeywords] = useState('');
    let [filteredPosts, setFilteredPosts] = useState([]);
    
    useEffect(() => {
        if(keywords === ''){
            setFilteredPosts([...posts]);
        }
    }, [keywords]);

    useEffect(() => {
        setFilteredPosts(posts)
    }, [posts]);
    
    const changeKeywords = (e) => {
        setKeywords(e.target.value)
    }


    
    const submitHandler = (e) => {
        e.preventDefault();
       
        const filter = posts.filter(p => {
            if(p.Title.includes(keywords) || p.Description.includes(keywords) || p.Tags.includes(keywords) || p.Contents.includes(keywords)){
                return p;
            }
        });
        setFilteredPosts(filter)
    }

    return (
        <>
            <div className="page-content" style={{marginBottom: '0'}}>
                <Form className="form-inline" onSubmit={submitHandler}>
                    <Form.Group>
                        <Form.Control type="text" style={{width: 500}} vaule={keywords} onChange={changeKeywords} placeholder="Input keywords"/>
                    </Form.Group>
                    <Form.Control type="submit" className="btn btn-primary btn-sm" style={{marginLeft: '1rem', fontWeight: 'bold'}} value="Search" />
                </Form>
            </div>
            <PostRoute posts={filteredPosts} Category={'Search'} />
        </>
    )
}

const mapStateToProps = (state) => ({
    posts: state.post.posts
})

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchPost)
