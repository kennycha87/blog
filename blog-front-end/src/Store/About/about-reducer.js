import { TYPES } from "./about-action-types";

const INITIAL_STATE = {
   picture:'',
   uploadPicture: '',
   mdData: ''
}

const aboutReducer = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case TYPES.LOAD_ABOUT:
            const data = action.payload;
            return {
                ...state, 
                picture: data.Picture,
                mdData: data.Describes
            }
        case TYPES.SET_MD_DATA:
            return {
                ...state, mdData: action.payload
            }
        case TYPES.SET_PICTURE:
            console.log(action.payload)
            return {
                ...state,
                profilePic: action.payload
            }
        case TYPES.SET_UPLOAD_PICTURE:
            return {
                ...state,
                uploadPicture: action.payload
            }
        case TYPES.CLEAR_UPLOAD_PICTURE:
            return {
                ...state,
                uploadPicture: ''
            }
        case TYPES.SET_PATH:
            return {
                ...state,
                picture: action.payload
            }
        default: 
            return {...state};
    }
}

export default aboutReducer;