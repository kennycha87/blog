const queriesloader = require('../Utils/queriesloader');
const sqlHandler = require('../Utils/mySqlHandler');

const post = queriesloader('Post');
const about = queriesloader('User');
const comment = queriesloader('Comment');

const getCategories = async (req, res) => {
  try {
    res.json(await sqlHandler(post.getCategories));
  } catch (error) {
    console.log(error);
    res.status(405).json();
  }
};

const getPosts = async (req, res) => {
  try {
    const posts = await sqlHandler(post.getPosts);
    const comments = await sqlHandler(comment.findComments);
    if (posts && comments) {
      const postWithComments = posts.map((p) => {
        const mapComment = comments.filter((c) => c.PostId === p.Id);
        if (mapComment.length > 0) {
          return {
            ...p,
            Comments: mapComment,
          };
        } else {
          return {
            ...p,
            Comments: [],
          };
        }
      });
      res.json(postWithComments);
    }
  } catch (error) {
    console.log(error);
    res.status(405).json();
  }
};

const getAboutMe = async (req, res) => {
  try {
    const result = await sqlHandler(about.aboutme);
    res.json(result[0]);
  } catch (error) {
    console.log(err);
    res.status(405).json();
  }
};

const findPost = async (req, res) => {
  try {
    const result = await sqlHandler(post.findPost, [
      parseInt(req.body.PostId),
      0,
    ]);
    if (result) {
      const comments = await sqlHandler(comment.findCommentsByPostId, {
        PostId: result[0].Id,
      });
      res.json({ ...result[0], Comments: comments });
    }
  } catch (error) {
    console.log(error);
    res.status(405).json();
  }
};

module.exports = {
  getCategories,
  getPosts,
  getAboutMe,
  findPost,
};
