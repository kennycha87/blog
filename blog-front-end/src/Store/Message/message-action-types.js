export const TYPES = {
    SET_SUCCESS: 'Set success message',
    SET_WARNING: 'Set warning message',
    SET_ERROR: 'Set error message',
    DISMISS: 'Cancel the message 10s after'
}