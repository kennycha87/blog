import React, {useState, useEffect} from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import URL from '../../../../config/url';
import { setError, setSuccess, setWarning} from '../../../../Store/Message/message-actions';
import { addComment } from '../../../../Store/Posts/post-actions';
import moment from 'moment';

export const NewComment = ({postId, addComment, setError, setSuccess, setWarning}) => {
    
    const [formData, setFormData] = useState({
        PostId: parseInt(postId),
        Name: '',
        Email: '',
        Comment: ''
    });
    
    useEffect(() => {
    }, [formData]);

    const submitComment = async event => {
        event.preventDefault();
        try {
            const result = await axios.post(URL + 'api/Comment', {Comment: formData});
            if(result.statusText === 'OK') {
                setSuccess('Thanks you, comment has been posted!');
                addComment({
                    ...formData,
                    Id: result.data.CommentId,
                    CreatedTime: moment(Date.now()).format('YYYY-MM-DDTHH:mm:ss.SSS')
                });
                setFormData({
                    ...formData,
                    Name: '',
                    Email: '',
                    Comment: ''
                });
            }
        } catch (error) {
            console.log(error);
        }
    }

    const changeName = e => {
        setFormData({
            ...formData,
            Name: e.target.value
        });
    }

    const changeEmail = e => {
        setFormData({
            ...formData,
            Email: e.target.value
        });
    }

    const changeComment = e => {
        setFormData({
            ...formData,
            Comment: e.target.value
        });
    }

    return (
        <div style={{marginBottom: '2rem'}}>
            <form onSubmit={submitComment}>
                <div className="form-group row">
                    <div className="col-sm-12" style={{textAlign: 'center', fontWeight: 600, fontSize:'1.2rem'}}>
                        New Comment
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label col-form-label-sm">Name:</label>
                    <div className="col-sm-8">
                        <input type="text" className="form-control form-control-sm " id="name" onChange={changeName} value={formData.Name} placeholder="Your Name" required/>
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label col-form-label-sm">Email:</label>
                    <div className="col-sm-8">
                        <input type="email" className="form-control form-control-sm" id="staticEmail" onChange={changeEmail} value={formData.Email} placeholder="Optional" />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label col-form-label-sm">Comment:</label>
                    <div className="col-sm-10">
                        <textarea className="form-control form-control-sm" rows="4" onChange={changeComment} value={formData.Comment} placeholder="Tell me what you think" />
                    </div>
                </div>
                <hr></hr>
                <div className="form-group row">
                    <div className="col-sm-12" style={{textAlign: 'right'}}>
                        <button className="btn btn-success btn-sm">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    )
}

const mapStateToProps = (state) => ({
    message: state.message,
})

const mapDispatchToProps = {
    setError, setSuccess, setWarning,
    addComment
}

export default connect(mapStateToProps, mapDispatchToProps)(NewComment)
