import { TYPES } from './post-action-types';

export const getPosts = data => {
    return {
        type: TYPES.GET_POSTS,
        payload: data
    }
}

export const addPost = post => {
    return {
        type: TYPES.ADD_POST,
        payload: post
    }
}

export const updatePost = post => {
    return {
        type: TYPES.UPDATE_POST,
        payload: post
    }
}

export const deletePost = id => {
    return {
        type: TYPES.DELETE_POST,
        payload: id
    }
}

export const getCategories = cates => {
    return {
        type: TYPES.GET_CATEGORIES,
        payload: cates
    }
}

export const addComment = comment => {
    return {
        type: TYPES.ADD_COMMENT,
        payload: comment
    }
}