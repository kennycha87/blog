import React from 'react';
import { connect } from 'react-redux';
import MDEditor from '@uiw/react-md-editor';
import { setMdData } from '../../../Store/About/about-actions';
import axios from 'axios';
import URL from '../../../config/url';
import {setError, setWarning, setSuccess} from '../../../Store/Message/message-actions';


export const AboutMdEditor = ({ about, setMdData, setError, setWarning, setSuccess }) => {

    const formSubmitter = e => {
        e.preventDefault();
        sendToServer();
    }

    const sendToServer = async () => {
        const token = localStorage.getItem('token');

        const option = { 
            headers: {
                "Authorization": token,
            }
        };

        axios.patch(URL + 'api/about/update', {Describes:about.mdData}, option)
            .then(res => {
                setSuccess(res.data.msg)
            })    
            .catch(err => {
                if(err.message.includes('401')){
                    setError('You are not logged in');
                }else{
                    setError('Server error! Please contact System Administrator');
                }
                
            });
    }
    

    return (
        <div>
            
            <form onSubmit={formSubmitter}>
                <MDEditor value={about.mdData} onChange={setMdData}/>
                <hr></hr>
                <button className="btn btn-primary" type="submit">Submit</button>
            </form>
        </div>
    )
}

const mapStateToProps = (state) => ({
    about: state.about,
})

// const mapDispatchToProps = dispatch => ({
//     a: data => dispatch(setMdData(data))
// });

const mapDispatchToProps = {
    setMdData,
    setError, 
    setWarning, 
    setSuccess
}

export default connect(mapStateToProps, mapDispatchToProps)(AboutMdEditor)
