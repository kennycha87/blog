const express = require('express');
const cors = require('cors');
const app = express();
const { v4: UUIDv4 } = require('uuid');
require('dotenv/config');

const verifyToken = require('./Utils/verifyToken');
const { json } = require('express');
// const fileUpload = require('express-fileupload');

const corsOptions = {
  origin: '*',
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
};

const PORT = 5000;

app.use(cors(corsOptions));
app.use(express.json());
// app.use(fileUpload());
app.use('/static', express.static(__dirname + '/Static'));

app.use('/', require('./Routes/home-route'));

app.use('/api/User', require('./Routes/user-route'));

app.use('/api/Post', verifyToken, require('./Routes/post-route'));
// app.use('/api/post', () => console.log('post'));

app.use('/api/LoadData', require('./Routes/load-data-route'));

app.use('/api/About', verifyToken, require('./Routes/about-route'));

app.use('/api/Comment', require('./Routes/comment-route'));

app.use('/api/upload', require('./Routes/upload-route'));

// listening
app.listen(PORT, console.log(`Server running on port: ${PORT}...`));
