const express = require('express');
const { addComment } = require('../Controller/comment-controller');

const router = express.Router();

router.put('/add', addComment);

module.exports = router;
