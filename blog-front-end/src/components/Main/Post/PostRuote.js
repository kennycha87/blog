import React, { useEffect } from 'react';
import PostCard from './PostCard';

const PostRoute = ({ posts, Category}) => {
    useEffect(() => {
        document.title = Category;
    }, []);
    return (
        <div className="page-content">
            <div className="row">
                {posts.map(post => (
                    <PostCard key={post.Id} post={post} />
                ))}
            </div>
        </div>
    )
}

export default PostRoute;
