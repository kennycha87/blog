import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import Publish from './Publish/Publish';
import Home from './Home/Home';
import Login from './Login/Login';
import ProtectedRoute from './ProtectedRoute';
import PageNotFound from './PageNotFound';
import Post from './Post/PostRuote';
import About from './About/About';
import PostDetails from './Post/PostDetail/PostDetails';
import PostEdit from './Post/PostDetail/PostEdit';
import Message from './Message';
import Search from './Post/SearchPost';

const Main = ( { allPosts, message }) => {
    const frontEndPosts = allPosts.filter(post => post.Category==='Front-End');
    const backEndPosts = allPosts.filter(post => post.Category==='Back-End');
    const databasePosts = allPosts.filter(post => post.Category==='Database');
    const otherPosts = allPosts.filter(post => post.Category==='Others');
    const [showMessage, setShowMessage] = useState(false);

    useEffect(() => {
        if(message.success !== null || message.error !== null || message.warning !== null){
            setShowMessage(true)
        }else{
            setShowMessage(false);
        }
    },[message]);

    return (
        <div className="main-content">
            {
                // defineshow message alert or not
                showMessage ? <Message/> : null
            }
            <Switch>
                <Route path="/" exact component={Home}/>
                <Route path="/Search" component={Search} />
                <Route path="/Post/:postId" exact component={PostDetails}/>
                <Route path="/Front-End" exact component={() => <Post posts={frontEndPosts} Category='Front-End'/>} />
                <Route path="/Back-End" exact component={() => <Post posts={backEndPosts} Category='Back-End' />} />
                <Route path="/Database" exact component={() => <Post posts={databasePosts} Category='Database' />} />
                <Route path="/Others" exact component={() => <Post posts={otherPosts} Category='Others' />} />
                <ProtectedRoute path="/Publish" component={Publish} />
                <ProtectedRoute path="/EditPost/:postId"  component={PostEdit}/>
                <Route path="/Login" component={Login} />
                <Route path="/About" component={About} />
                <Route component={PageNotFound}/>
            </Switch>
        </div>
    )
}
const mapStateToProps = state => ({
    allPosts: state.post.posts,
    message: state.message      
});

export default connect(mapStateToProps)(Main);
