import React, {useEffect} from 'react';
import { connect } from 'react-redux';
import PostRoute from '../Post/PostRuote';

export const Home = ({ posts }) => {
    useEffect(() => {
        document.title = 'Index';
    }, []);

    return (
        <>
            <h4 className='container' style={{textAlign: 'justify'}}>Welcome to my blog!! I hope the knowledge sharing helps, please feel free to drop me a message if you want to discuss the technical topic.</h4>
            <PostRoute posts={posts} />
        </>
    )
}

const mapStateToProps = (state) => ({
    posts: state.post.posts
})

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);