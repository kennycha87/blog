export const TYPES = {
    UPDATE_FORM_DATA: 'Update the form data',
    SET_TITLE: 'Set post title',
    SET_TAGS: 'Set post tag',
    SET_CATE:'Set post categories',
    SET_DESC: 'Set post description',
    SET_CONTENT: 'Set post content',
    SET_IMAGE: 'Set the post cover image',
    SEND_FORM: 'Send the form ',
}