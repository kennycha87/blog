import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { useParams, useHistory } from 'react-router-dom';
import Form from 'react-bootstrap/Form';
import MD from '@uiw/react-md-editor';
import Button from 'react-bootstrap/Button';
import axios from 'axios';
import { setSuccess, setError, setWarning} from '../../../../Store/Message/message-actions';
import URL from '../../../../config/url';
import { updatePost } from '../../../../Store/Posts/post-actions';

export const PostEdit = ({ posts, setSuccess, setError, setWarning, updatePost }) => {
    const {postId} = useParams();
    const [ completed, setCompleted ] = useState(false);
    const post = posts.filter(post => post.Id === parseInt(postId))[0];
    const [formData, setFormData] = useState(post);
    const [categories, setCategories] = useState([]);

    const history = useHistory();
    useEffect(() => {
        
        const getCates = async () => {
            const cates = axios.get(`${URL}api/LoadData/Categories`).catch(err => console.log(err));
            cates.then(res => setCategories(res.data));
        };
        getCates();
    },[]);

    useEffect(() => {
        if(formData.Contents.trim().length === 0 || formData.Title.trim().length === 0){
            setCompleted(false);
        }else{
            setCompleted(true)
        }
    },[formData]);
    
    const setTitle = (e) => {
        console.log(formData)
        setFormData({
            ...formData,
            Title: e.target.value
        })
    }

    const setTags = (e) => {
        setFormData({
            ...formData,
            Tags: e.target.value
        })
    }
    
    const setImage = (e) => {
        setFormData({
            ...formData,
            Image: e.target.files[0]
        });
        console.log(e.target.files[0])
    }

    const setDesc = (e) => {
        setFormData({
            ...formData,
            Description: e.target.value
        })
    }

    const setCate = (e) => {
        console.log(e.target.value)
        setFormData({
            ...formData,
            Category: e.target.value
        })
    }
    const setContent = (data) => {
        setFormData({
            ...formData,
            Contents: data
        })
    }

    const formSubmit = async (e) => {
        e.preventDefault();
        const token = await localStorage.getItem('token');
            const option = { 
                headers: {
                    "Authorization": token,
                    'Content-Type': 'multipart/form-data'
                }
            };
            const sendForm = new FormData();
            sendForm.append('file', formData.Image);
            sendForm.append('Id', postId);
            sendForm.append('Title', formData.Title);
            sendForm.append('Tags', formData.Tags);
            sendForm.append('Category', formData.Category);
            sendForm.append('Description', formData.Description);
            sendForm.append('Contents', formData.Contents);
            try {
                const result = await axios.patch( URL + 'api/post/updatePost', sendForm, option); 
                console.log(result);
                if(result.statusText === 'OK'){
                    setSuccess(result.data.msg);
                    updatePost(result.data.updatedPost);
                    history.push('/');
                }else{
                    console.log("something went wrong");
                } 
            } catch (error) {
                console.log(error.Error)
                setError(error.message);
            }
    }
    
    return (
        <div className="page-content">
            <h4 className="publishFormTitle">Edit Post</h4>
            <div style={{backgroundColor: 'white', borderRadius: '1rem'}}>
                <Form className="publishForm" onSubmit={formSubmit}>
                    <br></br>
                    <div className="row">
                        <div className="col-md-12">
                            <Form.Group controlId="postTitle">
                                <Form.Label>Post Title *</Form.Label>
                                <Form.Control type="text" value={formData.Title} onChange={setTitle} />
                                <Form.Text style={{color:  '#2167db'}}>Title of the post</Form.Text>
                            </Form.Group>
                        </div>
                        <div className="col-md-12">
                            <Form.Group controlId="Tags">
                                <Form.Label>Tags</Form.Label>
                                <Form.Control type="text" value={formData.Tags} onChange={setTags} />
                                <Form.Text style={{color:  '#2167db'}}>Input tags with '#'. e.g. #MongoDb</Form.Text>
                            </Form.Group>
                        </div>
                        <div className="col-md-6">
                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Description</Form.Label>
                                <Form.Control type="text" value={formData.Description} onChange={setDesc} />
                                <Form.Text style={{color:  '#2167db'}}>Brief description of the post</Form.Text>
                            </Form.Group>
                        </div>
                        <div className="col-md-6">
                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Category</Form.Label>
                                <Form.Control as="select" value={formData.Category} onChange={setCate}>
                                    { 
                                        categories.map( cat => <option key={cat.id} value={cat.id}>{cat.name}</option>)
                                    }
                                </Form.Control>
                                <Form.Text style={{color:  '#2167db'}}>Choose one category</Form.Text>
                            </Form.Group>
                        </div>
                        <div className="col-md-6">
                            <Form.Group>
                                <Form.Label>Cover Image</Form.Label>
                                <Form.File onChange={setImage} id="custom-file" label={formData.Image === undefined ? 'Upload Cover Image' : formData.Image.name} custom/>
                                <Form.Text style={{color:  '#2167db'}}>File type: .JPG, .JPEG or .PNG</Form.Text>
                            </Form.Group>
                        </div>
                    </div>
                    <Form.Group>
                        <Form.Label>Content *</Form.Label>
                        <MD value={formData.Contents} onChange={setContent}/>
                        <Form.Text style={{color:  '#2167db'}}>Input contents with MD format </Form.Text>
                    </Form.Group>

                    <hr></hr>
                    <Button variant="primary" type="submit" disabled={completed ? '' : 'disabled'}>
                        Update
                    </Button>
                    
                </Form>
            </div>
        </div>
    )
}

const mapStateToProps = (state) => ({
    posts: state.post.posts,
    message: state.message
})

const mapDispatchToProps = {
    setSuccess,
    setError,
    setWarning,
    updatePost
}

export default connect(mapStateToProps, mapDispatchToProps)(PostEdit)
