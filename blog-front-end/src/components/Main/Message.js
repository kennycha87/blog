import React, { useEffect } from 'react'
import { connect } from 'react-redux';
import Alert from 'react-bootstrap/Alert';
import { dismiss } from '../../Store/Message/message-actions';

export const Message = ({ error, success, warning, dismiss}) => {
    // set dismiss message 10s after
    useEffect(() => {
        if(error !== null || success !== null || warning !== null) {
            setTimeout( () => {
                dismiss();
            }, 10000)
        }
    },[error, success, warning]);

    return (
        <div className="row" style={{margin: '0 auto', width: '80%', marginTop: '2rem'}}>
            { 
                // error message
                error !== null && error !== '' ? 
                <Alert className="alert" variant="danger" onClose={dismiss} dismissible>
                    {error}
                </Alert>
                :
                null
            }
            
            { 
                // success message
                success !== null && success !== '' ? 
                <Alert variant="success" onClose={dismiss} dismissible>
                    {success}
                </Alert>
                :
                null
            }

            { 
                // warning message
                warning !== null && warning !== '' ? 
                <Alert variant="warning" onClose={dismiss} dismissible>
                    {warning}
                </Alert>
                :
                null
            }
        </div>
    )
}

const mapStateToProps = (state) => ({
    error: state.message.error,
    success: state.message.success,
    warning: state.message.warning
})

const mapDispatchToProps = {
    dismiss,
}

export default connect(mapStateToProps, mapDispatchToProps)(Message)
