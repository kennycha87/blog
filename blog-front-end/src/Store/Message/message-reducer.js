import { TYPES } from "./message-action-types";

const INITIAL_STATE = {
   success: null,
   warning: null,
   error: null
}

const messageReducer = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case TYPES.SET_SUCCESS:
            return {
                ...state,
                success: action.payload
            }
        case TYPES.SET_WARNING:
            return {
                ...state,
                warning: action.payload
            }
        case TYPES.SET_ERROR:
            return {
                ...state,
                error: action.payload
            }
        case TYPES.DISMISS:
            return {
                ...state,
                success: null,
                warning: null,
                error: null
            }
        default: 
            return {...state};
    }
}

export default messageReducer;