require('dotenv/config')
const {  MYSQL_HOST, MYSQL_PORT, MYSQL_DATABASE, MYSQL_USER, MYSQL_PASSWORD } = process.env;

const mysqlConfig =  {
    host: MYSQL_HOST,
    database: MYSQL_DATABASE,
    port: MYSQL_PORT,
    user: MYSQL_USER,
    password: MYSQL_PASSWORD
}

module.exports = mysqlConfig;