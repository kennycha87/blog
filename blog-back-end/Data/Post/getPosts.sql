select
    p.Id, Title, c.name as Category, Tags, Description, Contents, CoverImg
    , p.Status, p.CreatedTime, u.Alias as CreatedUser, p.LastModified, p.LastModifiedUser
from
    Posts p left join Categories c on p.Category = c.Id
    left join Users u on p.CreatedUser = u.Id
where
    p.Status <> 0;