import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';

const ProtectedRoute = ({ isLogin, component: Component, ...rest}) => {
    return (
        <Route {...rest} render={props => {
            return isLogin ? <Component {...props}/> : <Redirect to={{pathname: '/login'}} />
        }}/>
    )
};

const mapState = state => {
    return {
        isLogin: state.auth.isLogin
    }
}

export default connect(mapState)(ProtectedRoute);