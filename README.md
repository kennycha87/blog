# Person blog system with React.js Front-End and Express.js Back-End

`It is MIT license means you can use it for free to deployed to your production`

The project develops with MVVM design pattern with React.js and Express.js, using MySQL for the database solution.
please follow the below steps to deploy to your production environment:

#### 1. Requirements
    - A server with network connection
    - Node.js v10 installed
    - MySQL installed

#### 2. Clone the project from gitLab to the server

#### 3. Configure server system environment variable for MySQL and ports

#### 4. Run the database-creation.sql to create tables

#### 5. Navigate to the project folder on your server and run back-end and front-server
