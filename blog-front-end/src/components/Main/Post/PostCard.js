import React from 'react';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Card from 'react-bootstrap/Card';
import Image from 'react-bootstrap/Image';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import moment from 'moment';
import { Link } from 'react-router-dom';

import URL from '../../../config/url';

const useStyles = makeStyles({
    postCard:{
        marginTop: 30,
    },
    postImg: {
        maxWidth: '100%',
        maxHeight: 210
    },
    postTitle: {
        textAlign: 'center', 
        fontSize: 18, 
        fontWeight: 'bold', 
        color: '#2847a6'
    },
    postDesc: {
        fontWeight: 600,
        marginBottom: '20px'
    },
    postContents: {
        fontStyle: 'italic',
        height: 30,
    },
    postCategory: {
        fontStyle: 'italic',
        height: 30,
        textAlign: 'right'
    },
    postFooter: {
        bottom: 0, 
        position: 'absolute',
        width: '100%'
    },
    postTag:{
        marginBottom: '30px',
        padding: '5px 15px',
        
    },
    tags:{
        fontStyle: 'italic',
        fontWeight: 'bold',
        borderRadius: '3px;',
        backgroundColor: '#EDEDED',
    },
    postAuthor: {
        textAlign: 'right'
    }
});

export const PostCard = ({post}) => {
    const classes = useStyles();
    return (
        <div className="col-sm-12 col-md-12 col-lg-6" style={{}}>
            <Card className={classes.postCard}> 
                    <Card.Header>
                        <Link to={`/Post/${post.Id}`}>
                            <div className={classes.postTitle}>
                                {post.Title}
                            </div>
                        </Link>
                    </Card.Header>

                    <Card.Body>
                        <Row style={{height: '100%'}}>
                            <Col md={5} xs={12} style={{textAlign: 'center'}}>
                                <Link to={`/Post/${post.Id}`}>
                                <Image className={classes.postImg} src={ URL + 'Static/Images/' + post.CoverImg } />
                                </Link>
                            </Col>

                            <Col md={7} xs={12} style={{fontSize: '0.8rem'}}>
                                <Row>
                                    <Col xs={12} md={12} className={classes.postCategory}>
                                        <b>[{post.Category}]</b>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={12} md={12} className={classes.postDesc} >
                                        {
                                            post.Description.length > 100 ? 
                                            post.Description.substring(0, 155) + '...' : post.Description
                                        }
                                    </Col>
                                </Row>
                                <Row className={classes.postTag}>
                                    <Col xs={12} md={12} className={classes.tags}>
                                        {post.Tags}
                                    </Col>
                                </Row>

                                <Row className={classes.postFooter}>
                                    <Col md={6} xs={6}>
                                        {moment(post.CreatedTime).format('DD MMM, YYYY')}
                                    </Col>
                                    
                                    <Col md={6} xs={6} className={classes.postAuthor}>
                                        {post.CreatedUser}Kenny Leung
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
        </div>
    )
}

const mapStateToProps = (state) => ({
    
})

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(PostCard)
