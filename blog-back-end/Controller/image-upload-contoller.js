const { imageUpload } = require('../Utils/uploadHandler');
const imageType = '.jpg|.jpeg|.png';

const imageUploadHandler = async (req, res) => {
  const result = await imageUpload(req.file);
  if (result.success) {
    console.log(result);
    res.json(result);
  } else {
    res.json(result.err);
  }
};

module.exports = imageUploadHandler;
