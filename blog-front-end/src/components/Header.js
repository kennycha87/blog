import React from 'react';
import { useHistory } from "react-router-dom";
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav';
import { Link } from 'react-router-dom';


import GitHubIcon from '@material-ui/icons/GitHub';
import InfoIcon from '@material-ui/icons/Info';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { logout } from '../Store/Auth/auth-actions';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

const useStyles = makeStyles((theme) => ({
    toolbar: {
    //   borderBottom: `1px solid ${theme.palette.divider}`,
    },
    toolbarTitle: {
      fontWeight: 'bold',
      fontSize: '1.2rem'
    },
    toolbarTitleLink: {
        fontFamily: 'Gill Sans", sans-serif',
        fontWeight: 'bold',
        color: 'black',
        "&:hover": {
            fontWeight: 'bold',
            textDecoration: 'none',
            color: 'black'
        },
    },
    toggleDiv:{
        width: '100%', 
        textAlign: 'center'
    },
    toolbarLink: {
        flex: 1,
        fontWeight:'bold',
        textAlign: 'center',
        backgroundColor: '#4A4A4A',
        color: 'white',
        padding: '10px 10px',

        "&:hover": {
            backgroundColor: "#FFC250",
            fontWeight: 'bold',
            textDecoration: 'none',
            color: '#007391',
        },
        "&:active": {
            backgroundColor: '#FFC250'
        }
    },
    newPost: {
        backgroundColor: '#06FF9B',
        border: '2px solid',
        fontSize: '15px',
        color: 'white',
        fontWeight: 'bold',
        padding: '10px 10px',
        borderRadius: '10px',
        "&:hover": {
            fontWeight: 'bold',
            border: '2px solid #AEAEAE',
            textDecoration: 'none',
            color: '#007391',
        }
    }
  }));

const Header = ({ categories, auth, logout }) => {
    const classes = useStyles();
    const history = useHistory();

    const logoff = () => {
        logout();
        history.push("/login");
    }
    const toPublish = () => {
        history.push('/Publish')
    }

    return (
        <div>
            <Navbar collapseOnSelect expand="lg">
                <Navbar.Brand className={classes.toolbarTitle} href="/">KennyLeung Blog</Navbar.Brand>
                <Nav className="me-auto">
                        { 
                            auth.isLogin ? 
                            (<Button variant="contained" onClick={toPublish} size={"small"} color="primary">New Post</Button>)
                            : 
                            null
                        }
                    </Nav>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav" className="justify-content-end">
                    
                    <Nav>
                        <div className={classes.toggleDiv}>
                            <IconButton onClick={() => history.push('/Search')}>
                                <SearchIcon />
                            </IconButton>

                            <IconButton>
                                <GitHubIcon onClick={() => window.open('https://gitlab.com/users/kennycha87/projects')} />
                            </IconButton>

                            <IconButton onClick={() => history.push('/about')}>
                                <InfoIcon/>
                            </IconButton>

                            {
                                auth.isLogin ? 
                                (<IconButton onClick={logoff}>
                                    <ExitToAppIcon >Log off</ExitToAppIcon>
                                </IconButton>) 
                                :
                                (<IconButton>
                                    <AccountCircleIcon onClick={() => history.push('/Login')} />
                                </IconButton>)
                            }
                        </div>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>

             
             <Toolbar component="nav" variant="dense" style={{display: 'flex', padding: '0 0 0 0'}}>
                {categories.map((section, index) => (
                    <Link 
                        color="inherit"
                        noWrap
                        key={index}
                        to={"/"+section.name}
                        id={section.name}
                        className={classes.toolbarLink}
                        >
                        {section.name}
                        
                    </Link>
                ))}
                </Toolbar>
        </div>
    )
}

const mapStateToProps = state => ({
        auth: state.auth,
        categories: state.post.categories
    })

const mapDispatchToProps = {
    logout
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
