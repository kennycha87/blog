const fs = require('fs');
const { join } = require('path');

const loadSQLQueries =  (folderName) => {
    // set the folder location
    const filePath = join(process.cwd(), 'Data', folderName);
    console.log(filePath)

    // read file and to an array
    const files = fs.readdirSync(filePath, (err, data) => {
        return data
    });

    const sqlFiles = files.filter(f => f.endsWith('.sql'));
    const queries = {};

    sqlFiles.forEach( sqlFile => {
        const query = fs.readFileSync(join(filePath, sqlFile), {encoding: "UTF-8"});
        // add to queries object
        queries[sqlFile.replace(".sql", "")] = query;
    });
    return queries;
}

module.exports = loadSQLQueries;