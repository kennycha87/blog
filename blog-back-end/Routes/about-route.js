const express = require('express');
const {
  updateAbout,
  uploadPicture,
  updateProfilePicture,
} = require('../Controller/aboutController');
const multer = require('multer');

const router = express.Router();

const upload = multer();
const imagePath = `${process.cwd()}/Static/Images/`;

router.patch('/update', updateAbout);
router.patch('/update-profile-picture', updateProfilePicture);

module.exports = router;
