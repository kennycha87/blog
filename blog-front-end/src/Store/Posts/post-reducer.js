// rxreducer
import {TYPES} from './post-action-types'

const initialState = {
    posts: [],
    categories: []
}

const postReducer = (state = initialState, action) => {
    switch (action.type) {
        case TYPES.GET_POSTS:
            return {
                ...state, posts: action.payload
            }
        case TYPES.ADD_POST:
            return {
                ...state,
                posts: [...state.posts, action.payload]
            }
        case TYPES.UPDATE_POST:
            const newPostId = action.payload.Id
            const temp = state.posts.map(post => post.Id === newPostId ? action.payload : post);
            return {
                ...state,
                posts: temp
            }
        case TYPES.DELETE_POST:
            const tempPost = state.posts.filter(p => p.Id !== parseInt(action.payload));
            return {
                ...state,
                posts: tempPost
            }
        case TYPES.GET_CATEGORIES:
            return {
                ...state, 
                categories: action.payload
            }
        case TYPES.ADD_COMMENT:
            const newPosts = state.posts.map(p => {
                
                if(p.Id === action.payload.PostId){
                    p.Comments.push(action.payload)
                    return p
                }else{
                    return p
                }
            });
            console.log(newPosts)
            return {...state, posts: newPosts}
            
        default:
            return {
                ...state
            }
        }
}

export default postReducer;