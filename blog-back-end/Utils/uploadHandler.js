const fs = require('fs');
const { promisify } = require('util');
const { v4: UUIDv4 } = require('uuid');

const pipeline = promisify(require('stream').pipeline);
const imagePath = `${process.cwd()}/Static/Images/`;
const imageType = '.jpg|.jpeg|.png';

const imageUpload = async (image) => {
  const ext = image.detectedFileExtension;
  const fileName = (await UUIDv4()) + ext;

  if (!imageType.includes(ext)) {
    return { err: 'File is not image!' };
  } else {
    const filePath = imagePath + fileName;
    try {
      await pipeline(image.stream, fs.createWriteStream(filePath));
      return { success: 'File upload successful', image: fileName };
    } catch (err) {
      return { err };
    }
  }
};

module.exports = {
  imageUpload,
};
