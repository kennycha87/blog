import { TYPES } from "./auth-actions-types";

export const setAuth = (user) => {
    return {
        type: TYPES.SET_AUTH,
        payload: user
    }
}

export const logout = () => {
    return {
        type: TYPES.LOGOUT,
    }
}

export const setError = (err)=> {
    return { 
        type: TYPES.SET_ERROR,
        payload: err
    }
}