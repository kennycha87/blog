create table Users(
    Id int primary key auto_increment,
    UserID VARCHAR(20),
    Password varchar(100),
    Alias VARCHAR(50),
    Gender int,
    Birthday date,
    Email varchar(255),
    TelZone int,
    Tel int,
    CreatedTime timestamp default current_timestamp,
    CreateUser int default 1,
    LastModified timestamp default current_timestamp,
    LastModifiedUser int default 1,
    Remarks varchar(255)
);

create table Posts (
    Id int primary key auto_increment,
    Title VARCHAR(50),
    Category int,
    Tags VARCHAR(100),
    Description VARCHAR(100),
    Contents TEXT,
    Status tinyint default true,
    CreatedTime timestamp default current_timestamp,
    CreatedUser int,
    LastModified timestamp default current_timestamp,
    LastModifiedUser int
);

create table Categories(
    Id int primary key auto_increment,
    Name varchar(30),
    Status tinyint default 1,
    CreatedTime timestamp default current_timestamp,
    CreatedUser int default 1,
    LastModifiedDate timestamp default current_timestamp,
    LastModifiedUser int,
    Remarks varchar(255)
);