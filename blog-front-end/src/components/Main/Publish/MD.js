import React from 'react';
import MDEditor from '@uiw/react-md-editor';
import {Form} from 'react-bootstrap';
import { connect } from 'react-redux';
import { setContent } from '../../../Store/Publish/publish-actions';

const MD = ({ formData, setContent }) => {
    // onChange update state


    return (
        <Form.Group>
                <Form.Label>Content *</Form.Label>
                <MDEditor value={formData.Contents} onChange={setContent}/>
                <Form.Text style={{color:  '#2167db'}}>Input contents with MD format </Form.Text>
        </Form.Group>
    )
};

const mapState = state => {
    return {
        formData: state.publish.formData
    }
};

const mapDispatch = dispatch => {
    return {
        setContent: data => dispatch(setContent(data))
    }
}

export default connect(mapState, mapDispatch)(MD);
