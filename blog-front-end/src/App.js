import React, {useEffect} from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import Header from './components/Header';
import Main from './components/Main/Main';
import Footer  from './components/Footer';
import { BrowserRouter as Router } from 'react-router-dom';
import URL from './config/url';
import jwt from 'jsonwebtoken';

import { getCategories } from './Store/Posts/post-actions';
import { getPosts } from './Store/Posts/post-actions';
import { setAuth } from './Store/Auth/auth-actions';
import { loadAbout } from './Store/About/about-actions';
import { setSuccess, setError, setWarning } from './Store/Message/message-actions'; 

function App({ 
  getCategories,
  getPosts,
  setAuth,
  loadAbout,
  setSuccess,
  setWarning,
  setError }) {
  // page on load fetch data
  useEffect(() => {
    // fetch data when get into the page
    fetchAllData();
    document.title = 'Index';

    // read token and set auth when user refresh page
    const storedToken = localStorage.getItem('token');
    if(storedToken !== undefined && storedToken !== null){
      const token = storedToken.split(' ');
      const expired = jwt.decode(token[1]);

      if(expired.exp > Math.floor(Date.now()/1000)){
        setAuth({token: token[1]});
      }
    }
  },[]);

  // Declare all fetchAllData methods
  const  fetchAllData = async () => {

    try {
      //create method to fetch Navbar Categories
      const categories = await axios.get( URL + 'api/LoadData/Categories');
      getCategories(categories.data); 

      const allPosts = await axios.get(URL + 'api/LoadData/Posts');
      getPosts(allPosts.data);

      const aboutMe = await axios.get(URL + 'api/LoadData/AboutMe');
      loadAbout(aboutMe.data[0]);
    } catch (error) {
      setError(error.message)
    }
  }

  // render page
  return (
      <Router>
        <Header/>
          <Main/>
        <Footer/>
      </Router>
  );
}

// Map the redux dispatches to react props
const mapDispatchToProps = {
      getCategories,
      getPosts,
      setAuth,
      loadAbout,
      setSuccess,
      setWarning,
      setError
}

export default connect(null, mapDispatchToProps)(App);
