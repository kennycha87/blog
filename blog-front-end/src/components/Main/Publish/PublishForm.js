import React from 'react';
import {Form, Button} from 'react-bootstrap';
import axios from 'axios';
import { connect } from 'react-redux';
import {
    setTitle, 
    setCate, 
    setTags, 
    setDesc,
    setImage
} from '../../../Store/Publish/publish-actions';
import { addPost } from '../../../Store/Posts/post-actions'
import { setError, setSuccess, setWarning } from '../../../Store/Message/message-actions';
import MD from './MD';
import URL from '../../../config/url';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core';

const useStyle = makeStyles({
    publishForm: {
        padding: '10px 20px'
      }
});

const PublishForm = ({ 
    formData, userId, setTitle, setCate, setDesc, setTags, setImage, formCompleted, error,
    setSuccess, setError, setWarning, addPost, categories
}) => {

    const history = useHistory();
    const classes = useStyle();
    // submit form
    const formSubmit = event => {
        event.preventDefault();
        sendData();
    }

    const sendData = async () => {
        const token = await localStorage.getItem('token');
        const option = { 
            headers: {
                "Authorization": token,
                'Content-Type': 'multipart/form-data'
            }
        };
        const sendForm = new FormData();
        sendForm.append('file', formData.Image);
        sendForm.append('Title', formData.Title);
        sendForm.append('Tags', formData.Tags);
        sendForm.append('Category', formData.Category);
        sendForm.append('Description', formData.Description);
        sendForm.append('Contents', formData.Contents);
        try {
            const result = await axios.put(URL + 'api/post/create', sendForm, option); 
            console.log(result);
            if(result.statusText === 'OK'){
                setSuccess(result.data.msg);
                switch (parseInt(formData.Category)) {
                    case 1:
                        addPost({...formData, Category: 'Front-End',CoverImg:result.data.fileName, Id: result.data.insertId});
                        history.push('/');
                        break;
                    case 2:
                        addPost({...formData, Category: 'Back-End',CoverImg:result.data.fileName, Id: result.data.insertId});
                        history.push('/');
                        break;
                    case 3:
                        addPost({...formData, Category: 'Database',CoverImg:result.data.fileName, Id: result.data.insertId});
                        history.push('/');
                        break;
                    case 4:
                        addPost({...formData, Category: 'Others',CoverImg:result.data.fileName, Id: result.data.insertId});
                        history.push('/');
                        break;
                    default:
                        history.push('/');
                }
            }else{
                console.log("something went wrong")
            } 
        } catch (error) {
            console.log(error.Error)
            setError(error.message);
        }
    }

    return (
        <div style={{backgroundColor: 'white', borderRadius: '1rem'}}>
            <Form className={classes.publishForm} onSubmit={formSubmit}>
                <div className="row">
                    <div className="col-md-12">
                        <Form.Group controlId="postTitle">
                            <Form.Label>Post Title *</Form.Label>
                            <Form.Control type="text" value={formData.Title} onChange={setTitle} />
                            <Form.Text style={{color:  '#2167db'}}>Title of the post</Form.Text>
                        </Form.Group>
                    </div>
                    <div className="col-md-12">
                        <Form.Group controlId="Tags">
                            <Form.Label>Tags</Form.Label>
                            <Form.Control type="text" value={formData.Tags} onChange={setTags} />
                            <Form.Text style={{color:  '#2167db'}}>Input tags with '#'. e.g. #MongoDb</Form.Text>
                        </Form.Group>
                    </div>
                    <div className="col-md-6">
                        <Form.Group controlId="Description">
                            <Form.Label >Description</Form.Label>
                            <Form.Control type="text" value={formData.Description} onChange={setDesc} />
                            <Form.Text style={{color:  '#2167db'}}>Brief description of the post</Form.Text>
                        </Form.Group>
                    </div>
                    <div className="col-md-6">
                        <Form.Group controlId="Category">
                            <Form.Label>Category</Form.Label>
                            <Form.Control as="select" value={formData.Category} onChange={setCate}>
                                { 
                                    categories.map( cat => <option key={cat.id} value={cat.id}>{cat.name}</option>)
                                }
                            </Form.Control>
                            <Form.Text style={{color:  '#2167db'}}>Choose one category</Form.Text>
                        </Form.Group>
                    </div>
                    <div className="col-md-6">
                        <Form.Group>
                            <Form.Label>Cover Image</Form.Label>
                            <Form.File onChange={setImage} id="custom-file" label={formData.Image === undefined ? 'Upload Cover Image' : formData.Image.name} custom/>
                            <Form.Text style={{color:  '#2167db'}}>File type: .JPG, .JPEG or .PNG</Form.Text>
                        </Form.Group>
                    </div>
                </div>
                <MD/>    

                <hr></hr>
                <Button variant="primary" size="sm" type="submit" disabled={formCompleted ? '' : 'disabled'}>
                    Submit
                </Button>
                
            </Form>
        </div>
    )
};

const mapStateToProps = state => ({
        formData: state.publish.formData,
        error: state.publish.error,
        formCompleted: state.publish.formCompleted,
        userId: state.auth.profile.Id,
        categories: state.post.categories
    })

const mapDisPatchToProps = {
        setCate, 
        setTitle,
        setTags,
        setDesc,
        setImage,
        setSuccess,
        setError,
        setWarning,
        addPost
}

export default connect(mapStateToProps, mapDisPatchToProps)(PublishForm);
