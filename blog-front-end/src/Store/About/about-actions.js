import {TYPES} from './about-action-types';

export const loadAbout = data => {
    return {
        type: TYPES.LOAD_ABOUT,
        payload: data
    }
};

export const setMdData = data => {
    return {
        type: TYPES.SET_MD_DATA,
        payload: data
    }
}

export const setPicture = data => {
    return {
        type: TYPES.SET_PICTURE,
        payload: data.target.files[0]
    }
}

export const setUploadPicture = data => {
    return {
        type: TYPES.SET_UPLOAD_PICTURE,
        payload: data.target.files[0]
    }
}

export const clearUploadPicture = () => {
    return {
        type: TYPES.CLEAR_UPLOAD_PICTURE
    }
}

export const setPicturePath = link => {
    return {
        type: TYPES.SET_PATH,
        payload: link
    }
}
