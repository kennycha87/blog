const jwt = require('jsonwebtoken');

const secret_key = process.env.SECRET_KEY;

function verifyToken (req, res, next)  {
    console.log('Start verify token');
    const bearerHeader = req.headers['authorization'];
    //check if bearer is undefined
    if(typeof bearerHeader !== 'undefined' && bearerHeader.startsWith('Bearer ')){
        //split at the space
        const bearer = bearerHeader.split(' ');
        // req.token = bearer[1];
        jwt.verify(bearer[1], secret_key, (err, decoded) => {
            if(err){
                console.log(err)
                res.status(401).json();        
            }else{
                console.log('token valid')
                next();        
            }
        })
    }else{
        console.log('No token attached')
        res.status(401).json();
    }
}

module.exports = verifyToken;