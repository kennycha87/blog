import { TYPES } from "./publish-action-types";

const INITIAL_STATE = {
    formData: {
       Title: '',
       Tags: '',
       Category: 1,
       Description: '',
       Contents: '',
       Image: undefined
   },
   error: undefined,
   formCompleted: false
}

const publishReducer = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case TYPES.UPDATE_FORM_DATA:
            return {
                ...state,
            }
        case TYPES.SET_TITLE:
            // check if the required fields are not empty, set the form completion
            const isContentFilled = state.formData.Contents.trim().length
            if(action.payload.trim().length > 0 && isContentFilled > 0){
                return {
                    ...state, formData: {...state.formData, Title: action.payload}, 
                    formCompleted: true
                }
            }else{
                return {
                    ...state, 
                    formData: {...state.formData, Title: action.payload}, 
                    formCompleted: false
                }
            }

        case TYPES.SET_CATE:
            return {
                ...state, 
                formData: {...state.formData, Category: parseInt(action.payload)}
            }

        case TYPES.SET_TAGS:
            return {
                ...state, 
                formData: {...state.formData, Tags: action.payload}
            }

        case TYPES.SET_DESC:
            return {
                ...state, 
                formData: {...state.formData, Description: action.payload}
            }

        case TYPES.SET_CONTENT:
            // check if the required fields are not empty, set the form completion
            const isTitlefilled = state.formData.Title.trim().length
            if(action.payload.trim().length > 0 && isTitlefilled > 0){
                return {
                    ...state, 
                    formData: {...state.formData, Contents: action.payload}, 
                    formCompleted: true
                }
            }else{
                return {
                    ...state, 
                    formData: {...state.formData, Contents: action.payload}, 
                formCompleted: false}
            }
        case TYPES.SET_IMAGE:
            return {
                ...state, 
                formData: {
                    ...state.formData, Image: action.payload
                }
            }
        default: 
            return {...state};
    }
}

export default publishReducer;