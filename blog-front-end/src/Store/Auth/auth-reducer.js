import { TYPES } from "./auth-actions-types";
import jwt from 'jsonwebtoken';
// persistedState

let INITIAL_STATE = {
   isLogin: false,
   profile: {},
   error: undefined
}

const authReducer = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case TYPES.SET_AUTH:
            const jwtDecode = jwt.decode(action.payload.token);
            return {...state, isLogin: true, profile: jwtDecode}
        case TYPES.LOGOUT:
            const token = localStorage.getItem('token');
            if(token !== undefined || token !== null){
                localStorage.removeItem('token');
            }
            return {
                ...state, isLogin: false, profile: {}
            }
        case TYPES.SET_ERROR:
            return {...state, isLogin: false, error: action.payload.msg}

        default: 
            return {...state};
    }
}
export default authReducer;