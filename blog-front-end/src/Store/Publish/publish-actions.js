import { TYPES } from "./publish-action-types";

export const updateFormData = (data) => {
    return {
        type: TYPES.UPDATE_FORM_DATA,
        payload: data
    }
};

export const setTitle = (data) => {
    return {
        type: TYPES.SET_TITLE,
        payload: data.target.value
    }
};

export const setTags = (data) => {
    return {
        type: TYPES.SET_TAGS,
        payload: data.target.value
    }
};

export const setCate = (data) => {
    return {
        type: TYPES.SET_CATE,
        payload: data.target.value
    }
};

export const setDesc = (data) => {
    return {
        type: TYPES.SET_DESC,
        payload: data.target.value
    }
};

export const setContent = (data) => {
    return {
        type: TYPES.SET_CONTENT,
        payload: data
    }
};

export const setImage = (data) => {
    console.log(data.target.files[0])
    return {
        type: TYPES.SET_IMAGE,
        payload: data.target.files[0]
    }
};