import React, {useState} from 'react'
import { connect } from 'react-redux';
import { setImage } from '../../../Store/Publish/publish-actions';
import {Form, Button} from 'react-bootstrap';
import axios from 'axios';
import URL from '../../../config/url';

export const ImageForm = ({props}) => {
    const [file, setFile] = useState(undefined);
    const [fileName, setFileName] = useState('choose image');


    const changeImage = event => {
        const file = event.target.files[0];
        console.log(file)
       
        if(file.name.toLowerCase().includes("jpeg") || file.name.includes("jpg") || file.name.includes("png")){
            if(file.size > 2097152){
                alert('file too large')
            }else{
                setFile(file);
                setFileName(event.target.files[0].name);
            }
        }else{
            alert("The file is not supported, must jpeg/jpg/png")
        }
    }

    const uploadImage = async event => {
        event.preventDefault();
        const fileData = new FormData();
        fileData.append('file', file);
        try{
             axios.post(URL + 'api/fileUpload', fileData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            });
        }catch(err){
            console.log(err)
        }
    }

    return (
        <div className="row">
            <form className="form-inline" style={{marginLeft: '18px'}} onSubmit={uploadImage}>
                <div className="form-group mb-2">
                    <Form.Group controlId="file">
                        <Form.Control type="file" onChange={changeImage} />
                    </Form.Group>
                </div>
                <div className="form-group mb-2">
                    <Button variant="success" size="sm" type="submit">Upload</Button>
                </div>
                <img style={{width: '200px', height: '200px'}}  alt="1"/>
            </form>
            
        </div>
    )
}

const mapStateToProps = (state) => ({
    
})

const mapDispatchToProps = dispatch => ({
    setImage: data => dispatch(setImage(data))  
})

export default connect(mapStateToProps, mapDispatchToProps)(ImageForm)
