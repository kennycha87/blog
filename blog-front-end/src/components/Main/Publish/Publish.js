import { makeStyles } from '@material-ui/core';
import React, { useEffect } from 'react';
import PublishForm from './PublishForm';

const useStyle = makeStyles({
    publishFormTitle:{
        fontWeight: 'bold',
        fontSize: '25px',
        textAlign: 'center',
        color: '#09A4FF'
    }
});

const Publish = () => {
    useEffect(() => {
        document.title = 'New Post'
    }, []);
    const classes = useStyle();
    return (
        <div className="page-content">
            <h4 className={classes.publishFormTitle}>New Post</h4>

            {/* <ImageForm/> */}
            <PublishForm/>
        </div>
    )
}

export default Publish
