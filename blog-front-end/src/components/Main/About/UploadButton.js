import React, { useEffect } from 'react';
import PanoramaIcon from '@material-ui/icons/Panorama';
import { connect } from 'react-redux';
import { setPicture, setPicturePath, setUploadPicture, clearUploadPicture } from '../../../Store/About/about-actions';
import { setSuccess, setError, setWarning} from '../../../Store/Message/message-actions'
import axios from 'axios';
import URL from '../../../config/url'

const UploadButton = ({ about, setUploadPicture, clearUploadPicture, setPicturePath, setSuccess, setError, setWarning }) => {
    

    useEffect(() => {
        if(about.uploadPicture !== '' || about.uploadPicture.length !== 0){
            sendImageToServer();
        }
    },[about.uploadPicture]);


    const sendImageToServer = async () => {
        console.log('start')
        const token = await localStorage.getItem('token');
        console.log(token)
        const option = { 
            headers: {
                "Authorization": token,
                'Content-Type': 'multipart/form-data'
            }
        };
        const sendForm = new FormData();
        sendForm.append('file', about.uploadPicture);
        try {
            const result = await axios.post( URL + 'api/about/uploadPic', sendForm, option);
            if(result.statusText === 'OK'){
                setPicturePath(result.data.fileName);
                setSuccess('Picture has been uploaded!');
                clearUploadPicture();
            }
        } catch (error) {
            console.log(error);
            setError(error.message);
        }
    };

    return (
        <>
            <div className="pic-upload">
                <label className="btn btn-info">
                    <input onChange={setUploadPicture} style={{display:'none'}} type="file"/>
                    <span style={{fontWeight: 'bold', fontSize: '12px'}}><PanoramaIcon fontSize="small"/> Upload</span>
                </label>
                
            </div>
        </>
    )
}

const mapStateToProps = state => ({
    about: state.about
})

const mapDispatchToProps = {
    setPicture,
    setUploadPicture,
    clearUploadPicture,
    setPicturePath,
    setSuccess,
    setError,
    setWarning
}

export default connect(mapStateToProps, mapDispatchToProps)(UploadButton)
