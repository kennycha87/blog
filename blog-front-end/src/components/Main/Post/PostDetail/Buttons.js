import React, { useState } from 'react';
import { connect } from 'react-redux';
import Button from 'react-bootstrap/Button';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory, useParams } from 'react-router-dom';
import Modal from 'react-bootstrap/Modal';
import axios from 'axios';
import URL from '../../../../config/url';
import { setSuccess, setError, setWarning } from '../../../../Store/Message/message-actions';
import { deletePost } from '../../../../Store/Posts/post-actions';

const useStyle = makeStyles( theme => ({
    edit: {
        marginRight: '1rem'
    },
    delete: {
        marginRight: '1rem'
    }
}));

export const Buttons = ({ setSuccess, setError, setWarning, message, deletePost }) => {
    const history = useHistory();
    const classes = useStyle();
    const [show, setShow] = useState(false);
    const { postId } = useParams();

    const potDelete = async () => {
        const token = await localStorage.getItem('token');
        const option = { 
            headers: {
                "authorization": token
            },
            data: {
                "Id": postId   
            }
        };
        try {
            const result = await axios.delete(URL + 'api/post/deletePost', option);
            if(result.statusText === 'OK'){
                deletePost(postId);
                setSuccess(result.data.msg);
                history.push('/');
            }else{
                
            }
        } catch (error) {
            setError(error.message);
        }
    }

    return (
        <div className="row" style={{justifyContent: 'flex-end'}}>
            <Button variant="primary" className={classes.edit} onClick={() => history.push(`/EditPost/${postId}`)}><b>Edit</b></Button>
            <Button variant="danger" className={classes.delete} onClick={() => setShow(true)} ><b>Delete</b></Button>

            <Modal aria-labelledby="contained-modal-title-vcenter" animation={false} centered show={show} onHide={() => setShow(false)}>
                <Modal.Header closeButton/>

                <Modal.Body>
                    Are you sure want to delete the post?
                </Modal.Body>

                <Modal.Footer>
                    <Button variant="secondary" onClick={() => setShow(false)}>Close</Button>
                    <Button variant="primary" onClick={potDelete}>Delete</Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}

const mapStateToProps = (state) => ({
    message: state.message
})

const mapDispatchToProps = {
    setError,
    setSuccess,
    setWarning,
    deletePost
}

export default connect(mapStateToProps, mapDispatchToProps)(Buttons)
