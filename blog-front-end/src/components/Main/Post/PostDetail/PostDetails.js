import React from 'react';
import { useParams } from 'react-router';
import { connect } from 'react-redux';
import Post from './Post';
import { makeStyles } from '@material-ui/core/styles';
import Buttons from './Buttons';
import CommentMain from '../PostComment/CommentMain';

const useStyle = makeStyles( theme => ({
    post:{
        width: '80%',
        margin: '0 auto',
        backgroundColor: 'white',
        borderRadius: '0.8rem',
        padding: '15px 20px',
        marginTop: '1rem',
        marginBottom: '1rem'
    },
})
)

export const PostDetails = ({ auth }) => {
    let {postId} = useParams();
    const classes = useStyle();
    return (
        <>
            <div className={classes.post}>
                { auth.isLogin ? <Buttons/> : null}
                <Post postId={postId} />
                
            </div>
            <div className={classes.post} style={{marginBottom: '4rem'}}>
                <CommentMain/>
            </div>
            
        </>
        
    )
}

const mapStateToProps = (state) => ({
    auth: state.auth,
});

export default connect(mapStateToProps)(PostDetails)