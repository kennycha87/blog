export const TYPES = {
    SET_AUTH: 'Set authorization when user login successful',
    LOGOUT: 'Logout',
    SET_ERROR: 'Set the error message'
}