const express = require('express');
const { userLogin, createUser } = require('../Controller/userController');
const jwt = require('jsonwebtoken');

const router = express.Router();

router.get('/')

router.post('/login', userLogin);

router.post('/create', createUser);

module.exports = router;