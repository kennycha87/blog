const express = require('express');
const multer = require('multer');
const imageUploadHandler = require('../Controller/image-upload-contoller');

const router = express.Router();

router.post('/image_upload', multer().single('file'), imageUploadHandler);

module.exports = router;
