const queriesloader = require('../Utils/queriesloader');
const sqlHandler = require('../Utils/mySqlHandler');

const query = queriesloader('Comment');

const addComment = async (req, res) => {
  console.log(req.body);
  try {
    const result = await sqlHandler(query.addComment, { ...req.body.Comment });
    if (result.affectedRows > 0) {
      res.json({ msg: 'Comment posted', CommentId: result.insertId });
    } else {
      res.status(500).json();
    }
  } catch (error) {
    console.log(error);
    res.status(500).json();
  }
};

module.exports = {
  addComment,
};
