const mysql = require('mysql');
const mysqlConfig = require('../Config/mysql-config');

const query = (sql, params) => {
  return new Promise((resolve, reject) => {
    const db = mysql.createConnection(mysqlConfig);

    // start connection
    db.connect((err) => {
      // connect DB
      if (err) {
        throw err;
      }
    });

    db.query(sql, params, (err, rows) => {
      // start query
      if (err) {
        reject(err);
      } else {
        resolve(rows);
      }
    });
    // close connection
    db.end();
  });
};

module.exports = query;
