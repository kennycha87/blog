import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { setAuth, setError } from '../../../Store/Auth/auth-actions'
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router-dom";
import md5 from 'md5';
import axios from 'axios';
import URL from '../../../config/url';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

// function start
const LoginForm = ({auth, setAuth, setError}) => {
  const classes = useStyles();
  const history = useHistory();

  const [loginForm, setLoginForm] = useState({
    UserId: '',
    Password: ''
  });

  const [disabled, setDisabled] = useState(true)

  useEffect(() => {
      if(loginForm.UserId.length !== 0 && loginForm.Password.length !== 0){
        setDisabled(false);
      }else{
        setDisabled(true);
      }       
    }, [loginForm])
    
  useEffect(() => {
    if(auth.isLogin === true ) history.push('/');
  },[auth]);

  const setId = event => {
    setLoginForm({
      ...loginForm, 
      UserId: event.target.value
    });
    setError({msg: undefined});
  };

  const setPassword = event => {
    event.target.value.length === 0 ? setLoginForm({
      ...loginForm,
      Password: ''
    })
    :
    setLoginForm({
      ...loginForm, 
      Password: md5(event.target.value)
    });
    setError({msg: undefined});
  };

  const loginSubmit = async event => {
    event.preventDefault();
    const result = axios.post( URL + 'api/user/login', loginForm).catch(err => {
      console.log(err);
      setError({msg: 'UserId or Passowrd is incorrect!'});
    })
    
    await result.then(res => {
        
      if(res !== undefined){
        setAuth(res.data);
        localStorage.setItem('token', 'Bearer ' + res.data.token);
      }
    });
  };

  return (
      <form className={classes.form} noValidate>
        <TextField
          variant="outlined"
          margin="normal"
          id="id"
          fullWidth
          label="Login ID"
          autoComplete="Login ID"
          autoFocus
          value={loginForm.UserId}
          onChange={setId}
          required
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          label="Password"
          type="password"
          autoComplete="current-password"
          id="password"
          value={loginForm.password}
          onChange={setPassword}
        />
        <FormControlLabel
          control={<Checkbox value="remember" color="primary" />}
          label="Remember me"
        />
        {auth.error === undefined ? <></>: <div style={{width: '100%', backgroundColor: "#FF6E65", padding: '8px 8px', borderRadius: "5px", textAlign: 'center'}}>{auth.error}</div>}
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          className={classes.submit}
          onClick={loginSubmit}
          disabled={disabled}
        >
          Sign In
        </Button>

        
      </form>
  )
}

const mapDispatch = dispatch => {
  return {
    setAuth: user => dispatch(setAuth(user)),
    setError: err => dispatch(setError(err))
  }
}

const mapState = state => {
  return {
    auth: state.auth
  }
}

export default connect(mapState, mapDispatch)(LoginForm)
