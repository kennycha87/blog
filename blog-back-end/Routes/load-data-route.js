const express = require('express');
const { getCategories, getPosts, getAboutMe, findPost } = require('../Controller/loadDataController');

const router = express.Router();

router.get('/Categories', getCategories);

router.get('/Posts', getPosts);

router.get('/AboutMe', getAboutMe);

router.post('/FindPost', findPost);

module.exports = router;