import React from 'react';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import Card from 'react-bootstrap/Card';
import Image from 'react-bootstrap/Image';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import moment from 'moment';
import URL from '../../../config/url';

const useStyles = makeStyles({
    postCards: {
        width: '80vw',
        margin: '0 auto',
        height: '20vh',
        padding: '20px 20px',
        display: 'flex',
        justifyItems: 'center',
        alignItems: 'center',
        marginTop: '20px'
    },
    postImg: {
        width: 270
    },
    postTitle: {
        textAlign: 'center', 
        fontSize: 18, 
        fontWeight: 'bold', 
        color: '#2847a6'
    },
    postDesc: {
        fontSize: '1.1rem', 
        fontWeight: 600
    },
    postContents: {
        fontStyle: 'italic'
    },
    postFooter: {
        width: 'inherit', 
        bottom: 0, 
        position: 'absolute'
    },
    postTag:{
        fontStyle: 'italic'
    },
    postAuthor: {
        textAlign: 'right'
    }
})

const LatestPost = ({posts}) => {
    const classes = useStyles();

    return (
        <>
            {posts.map(post => (
                <Card key={post.Id}> 
                    <Card.Header>
                        <div className={classes.postTitle}>aslkdjasd alksjdklasd sdlksjdkiuduedbv hjdnfcn 9isdip sdo spodmxcs -ofspod</div>
                    </Card.Header>
                    <Card.Body>
                        <Row>
                            <Col md={4} xs={6} style={{textAlign: 'center'}}>
                                <Image className={classes.postImg} src={URL + 'Static/Images/'  + post.CoverImg } />
                            </Col>
                            <Col md={8} xs={6}>
                                <Row>
                                    <Col xs={12} md={12} className={classes.postDesc}>
                                        {post.Description}
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={12} md={12} className={classes.postContents}>
                                        {post.Contents.substring(0, 255)+'...'}
                                    </Col>
                                </Row>
                                <Row className={classes.postFooter}>
                                    <Col xs={12} md={12} className={classes.postTag}>
                                        {post.Tags}
                                    </Col>
                                    <Col md={6} xs={6}>
                                        {moment(post.CreatedTime).format('DD-MMM-YYYY')}
                                    </Col>
                                    <Col md={6} xs={6} className={classes.postAuthor}>
                                        {post.CreatedUser}Kenny Leung
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
            ))}
        </>
    )
}

const mapStateToProps = state => {
    return {
        posts: state.post.posts
    }
}

export default connect(mapStateToProps )(LatestPost);
