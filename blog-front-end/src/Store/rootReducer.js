import { combineReducers } from "redux";
import auth from './Auth/auth-reducer';
import publish from './Publish/publish-reducer';
import post from './Posts/post-reducer';
import about from './About/about-reducer';
import message from './Message/message-reducer';

const rootReducer = combineReducers({
    auth,
    publish,
    post,
    about,
    message
});

export default rootReducer;