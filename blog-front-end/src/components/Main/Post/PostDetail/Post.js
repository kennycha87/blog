import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import MDEditor from '@uiw/react-md-editor';
import { makeStyles } from '@material-ui/core/styles';
import URL from '../../../../config/url';
import moment from 'moment';

const useStyle = makeStyles( theme => ({
    post:{
        width: '80%',
        margin: '0 auto',
        backgroundColor: 'white',
        borderRadius: '0.8rem',
        padding: '15px 20px',
    },
    postTitle: {
        textAlign: 'center',
        fontSize: '1.8rem',
        fontWeight: 'bold',
        marginTop: '1rem'
    },
    image: {
        maxWidth: '60%',
        maxHeight: '200px',
        margin: '0 auto',
        textAlign: 'center',
        borderRadius: '0.8rem',
    },
    tags: {
        fontStyle: 'italic',
        width: '100%',
        textAlign: 'center',
        paddingLeft: '15px',
        paddingRight: '15px',
        marginTop: '2rem'
    },
    
}));


export const Post = ({ posts, postId }) => {
    const classes = useStyle();

    const [post, setPost] = useState([]);
    console.log(post)

    // onLoad page fetch post
    useEffect( () => {
        const p = posts.filter(p => p.Id === parseInt(postId));
        if(p.length !== 0){
            setPost(p);
        }
    },[posts]);

    return (
        post === undefined || post === null || post === {} ? 
            null
        :
        <>
            {
                post.map(post => (
                    <div key={post.Id}>
                        <div className="row">
                            <div className="col-md-12" style={{textAlign: 'center'}}>
                                <img className={classes.image} src={URL + 'Static/Images/' + post.CoverImg } alt=""></img>
                            </div>
                            <div className="col-md-12" >
                                <div className={classes.postTitle}>
                                    {post.Title}
                                </div>
                            </div>
                            
                            <div className="col-md-6" style={{marginTop: '2rem'}}>
                                <b>Created:</b> {moment(post.CreatedTime.substring(0, 22)).format('HH:mm:ss | DD MMM, YYYY')}
                                {console.log(post)}
                            </div>
                            <div className="col-md-6 text-right" style={{marginTop: '2rem'}}>
                                <b>Last Modified:</b> {moment(post.LastModified.substring(0, 22)).format('HH:mm:ss | DD MMM, YYYY')}
                            </div>
                            <div className={classes.tags}>
                                <i>{post.Tags}</i>
                            </div>
                            
                        </div>
                        <MDEditor.Markdown source={post.Contents} />
                    </div>
                ) )
            }
        </>
    )
}

const mapStateToProps = (state) => ({
    auth: state.auth,
    posts: state.post.posts,
})

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(Post)