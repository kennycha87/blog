import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import UploadButton from './UploadButton'
import MDEditor from '@uiw/react-md-editor';
import AboutMdEditor from './AboutMdEditor';
import URL from '../../../config/url'

const About = ({ isLogin, about, setErr, setSuccess }) => {

    useEffect(() => {
        document.title = 'About'
    }, []);
    
    return (
        <div className="page-content">
            <div className="profile-pic">
                <img src={`${URL}Static/Images/${about.picture }`} alt="Kenny Leung"/>
                { isLogin === true ? <UploadButton/> : null}
            </div>
                
            { isLogin ? 
                <AboutMdEditor/>
                :
                <MDEditor.Markdown source={about.mdData} />
            }    
        </div>
    )
}

const mapStateToProps = state => ({
    isLogin: state.auth.isLogin,
    about: state.about
});

export default connect(mapStateToProps)(About);
