export const TYPES = {
    GET_POSTS: 'Get all the post from back-end',
    ADD_POST: 'Add Post',
    UPDATE_POST: 'Update current post',
    DELETE_POST: 'Delete the post by Id',
    GET_CATEGORIES: 'Get the categories',
    ADD_COMMENT: 'Add comment to post'
}