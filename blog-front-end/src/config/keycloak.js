import Keycloak from 'keycloak-js';
const keycloakConfig = {
  url: 'http://192.168.0.150:8081/auth',
  realm: 'Spends',
  clientId: 'front-end',
};
const keycloak = new Keycloak(keycloakConfig);
export default keycloak;
