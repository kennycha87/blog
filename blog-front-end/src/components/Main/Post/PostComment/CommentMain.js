import React from 'react'
import { connect } from 'react-redux';
import { useParams } from 'react-router';
import { makeStyles } from '@material-ui/core/styles';

import NewComment from './NewComment';
import Comments from './Comments';

const useStyle = makeStyles( theme => ({
    commentBlock: {
        backgroundColor: 'white',
    }
}));

export const CommentMain = () => {
    const classes = useStyle();
    const { postId } = useParams();

    return (
        <div className={classes.commentBlock}>
            <NewComment postId={postId} />
            <Comments postId={postId}/>
        </div>
    )
}

const mapStateToProps = (state) => ({
    
})

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(CommentMain)
