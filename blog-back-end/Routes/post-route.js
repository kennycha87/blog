const express = require('express');
const {
  getPosts,
  getCategories,
  createPost,
  deletePost,
  updatePost,
  getPostById,
} = require('../Controller/postController');

const router = express.Router();

router.get('/', getPosts);

router.put('/create', createPost);

router.get('/getCategories', getCategories);

router.post('/getPostById', getPostById);

router.delete('/deletePost', deletePost);

router.patch('/updatePost', updatePost);

module.exports = router;
