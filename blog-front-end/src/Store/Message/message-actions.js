import { TYPES } from "./message-action-types";

export const setSuccess = (msg) => {
    return {
        type: TYPES.SET_SUCCESS,
        payload: msg
    }
}

export const setWarning = (msg) => {
    return {
        type: TYPES.SET_WARNING,
        payload: msg
    }
}

export const setError = (msg) => {
    return {
        type: TYPES.SET_ERROR,
        payload: msg
    }
}

export const dismiss = () => {
    return {
        type: TYPES.DISMISS,
    }
}